package com.prodapt.remedy.service;


import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;



import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import com.prodapt.remedy.common.MysqlCon;





/**
 * 
 * @author gandhi.d
 *
 */
@Path("/")
public class TokenService {
	
	private static Logger log=Logger.getLogger(TokenService.class);
	
	
	
	@GET
	@Path("/user")
	public Response getUser(@QueryParam("name") String name,@QueryParam("password") String password) {
		JSONObject jsonObject=new JSONObject();
		JSONObject userObject=new JSONObject();
		String success="failed";
		try{
			Connection con=MysqlCon.getConnection();
			PreparedStatement preStatement=con.prepareStatement("select * from user where name=? and password=?");
			preStatement.setString(1, name);
			preStatement.setString(2, password);
			ResultSet resultSet=preStatement.executeQuery();
			while(resultSet.next()){
				success="success";
				jsonObject.put("name", resultSet.getString(1));
				jsonObject.put("password", resultSet.getString(2));
			}
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		userObject.put("user", jsonObject);
		userObject.put("success", success);
		userObject.put("status", 200);
		System.out.println(userObject.toString());
		
		return Response.status(200).entity(userObject.toString()).build();
	}
	
	
	

	
	
	public <E, T> E getPojo(String jsonData, Class<E> clazz) {
		E obj = null;
		try {
			obj = clazz.newInstance();
			ObjectMapper mapper = new ObjectMapper();
			mapper.readValue(jsonData, obj.getClass());
			
			Class cls = obj.getClass();
			obj = (E) mapper.readValue(jsonData, obj.getClass());;
		} catch (IllegalAccessException | InstantiationException | NullPointerException | IOException ex) {
			log.info("Exception while parsing Input Json : " + ex);
		}
		return obj;
	}

	
	
	


}
