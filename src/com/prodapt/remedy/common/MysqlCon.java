package com.prodapt.remedy.common;

import java.sql.*;

/**
 * 
 * @author gandhi.d
 *
 */

public class MysqlCon {
	private static Connection connection=null;
	
	private MysqlCon(){
		
	}
	
	public static Connection getConnection(){
		if(connection==null){
			try {
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/remedy", "root", "root");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return connection;
	}
	
	
	public static void closeConnection(){
		if(connection!=null){
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws SQLException {
		Connection connection=MysqlCon.getConnection();
		
		Statement statement=connection.createStatement();
		
		ResultSet resultSet=statement.executeQuery("select * from student");
		
		while(resultSet.next()){
			System.out.println("name "+resultSet.getString("name"));
		}
		
	}
	
	
	
}