package com.prodapt.remedy.common;

import java.io.UnsupportedEncodingException;

/**
 * 
 * @author gandhi.d
 *
 */
public class CustomQuizException extends Exception {

	
	private static final long serialVersionUID = 54461L;

	public CustomQuizException() {

	}

	public CustomQuizException(String message) {
		super(message);
	}
	

	
	public CustomQuizException(IllegalArgumentException exception) {
		super(ResponseMessage.INVALID_TOKEN);
	}
	
	public CustomQuizException(UnsupportedEncodingException exception) {
		super(ResponseMessage.INVALID_TOKEN);
	}
	
	public CustomQuizException(RuntimeException exception) {
		super(ResponseMessage.INVALID_TOKEN);
	}
	
	

}
