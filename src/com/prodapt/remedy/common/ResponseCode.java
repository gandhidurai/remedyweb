package com.prodapt.remedy.common;

/**
 * 
 * @author gandhi.d
 *
 */
public class ResponseCode {
	
	public static final Integer SUCCESS=200;
	public static final Integer INTERNAL_SERVER_ERROR=500;
	public static final Integer BAD_REQUEST=400;
	public static final Integer FORBIDDEN=403;
	public static final Integer UNAUTHORIZED=401;
	

}
